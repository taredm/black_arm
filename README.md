# black_arm

ELMO driver와 CAN통신을 사용하는 7축 로봇팔의 제어

## Installation

### Xenomai

xenomai3 mercury를 사용함. Cobalt가 성능은 더 좋지만 kernel compile이 필요하여 설정이 어렵고, 많은 경우 mercury도 충분한 성능이 나옴.
```sh
git clone git://git.xenomai.org/xenomai.git
cd xenomai-3
./scripts/bootstrap
./configure --with-core=mercury --enable-pshared --enable-smp
make
sudo make install
```

빌드 및 설치 확인
```sh
sudo /usr/xenomai/bin/latency
```
레이턴시가 안정적으로 잘 나오는지 확인

### Build from source

* Prerequisites
    * CAN: Kvaser CANLib
    * Python3: PySide for GUI, pyqtgraph for visualization

* Build

    ```sh
    git clone https://gitlab.com/taredm/black_arm.git
    cd black_arm
    cmake .
    make
    ```

## How to Use

## Elmo and CanOpen

## Xenomai