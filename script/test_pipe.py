'''
This program works with "test_xeno_pipe"
'''

import os, time, sys
import struct
pipe_name = '/tmp/pipe_test'

if not os.path.exists(pipe_name):
    print('\'test_xeno_pipe\' should be running...')
    exit()  

pipein = open(pipe_name, 'r+b', 0)
q = 0.0
din = 0
timestamp = 0.0

try:
  while True:
    n = struct.unpack('IIdd', pipein.read(24))    # Read 8+8+4=20 byte
    #pipein.seek(0) 
    if n[0]==0:
        q=n[2]
        timestamp = n[3]
    elif n[0]==1:
        din=n[1]
        timestamp = n[3]

    print("%7.3lf[deg] at %7.3lf[sec] : %s  -" % (q, timestamp, bin(din)), end ="\r")
    # 0b1111110000000000000000

except KeyboardInterrupt:
    pipein.close()
    print("Exit")