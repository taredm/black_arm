import os
import sys
import signal
import time
import struct
import math
import numpy as np
import pandas as pd

from PySide import QtCore, QtGui, QtUiTools
import pyqtgraph as pg



class Worker(QtCore.QThread):
    #signal
    recvMsg = QtCore.Signal(object)

    def __init__(self, parent=None):
        QtCore.QThread.__init__(self, parent)

        self.EXIT_FLAG = False
        pipe_name = '/tmp/pipe_test'

        if not os.path.exists(pipe_name):
            print('\'test_xeno_pipe\' should be running...')
            self.EXIT_FLAG = True

        self.pipein = open(pipe_name, 'r+b', 0)


    def run(self):
        q = 0.0
        din = 0
        timestamp = 0.0
        msg = {'time':0.0, 'joint':0.0, 'din':False}

        while not self.EXIT_FLAG:
            # recieve pipe msg
            try:
                n = struct.unpack('IIdd', self.pipein.read(24))    # Read 8+8+4=20 byte
                #pipein.seek(0) 
                if n[0]==0:
                    q=n[2]
                    timestamp = n[3]
                elif n[0]==1:
                    din=n[1]
                    timestamp = n[3]

                #print("%7.3lf[deg] at %7.3lf[sec] : %s  -" % (q, timestamp, bin(din)), end ="\r")
                msg['time'] = timestamp
                msg['joint'] = q
                msg['din'] = bool(din & 0b100)                

            except:
                print("Read pipe error!")
                self.EXIT_FLAG = True

            # qApp.lock()
            self.recvMsg.emit(msg)
            # qApp.unlock()
        
    def stop(self):
        print("stop....")
        self.EXIT_FLAG = True
        self.pipein.close()         




class MyWidget(QtGui.QWidget):
    ''' my widget '''

    def __init__(self, *args):
        QtGui.QWidget.__init__(self)

        self.time_data = []
        self.joint_data = []
        self.din_data = []
        self.offset_sum = 0
        self.offset_count = 0

        # load ui file
        ui_file = os.path.join(os.path.dirname(__file__), 'set_home_pos.ui')
        ui = self.loadUI(ui_file)
        self.lineEdit = ui.lineEdit
        self.offsetLabel = ui.offsetLabel

        # set pyqtgrasph widget
        self.pgWidget = pg.PlotWidget(name='JointAngle')
        self.jointPlot = self.pgWidget.plot()
        self.jointPlot.setPen((200,200,100))
        #self.pgWidget.setXRange(0,10)
        self.pgWidget.setYRange(-30,30)
        self.pgWidget.setMouseEnabled(x=True, y=False)

        viewLayout = QtGui.QVBoxLayout()
        viewLayout.addWidget(self.pgWidget)
        ui.viewGroupBox.setLayout(viewLayout)

        self.updatePlot()

        # start thread
        self.worker = Worker(self)
        self.worker.recvMsg.connect(self.processMsg)
        self.worker.start()


    def loadUI(self, ui_filename):
        loader = QtUiTools.QUiLoader()
        ui_qfile = QtCore.QFile(ui_filename)
        ui_qfile.open(QtCore.QFile.ReadOnly)
        ui_obj = loader.load(ui_qfile, self)
        ui_qfile.close()
        return ui_obj


    def updatePlot(self):
        self.jointPlot.setData(y=self.joint_data[-5000:], x=self.time_data[-5000:])

        
    def addData(self, time, joint_angle, digital_input):
        self.time_data.append(time)
        self.joint_data.append(joint_angle)
        self.din_data.append(digital_input)

        n = len(self.din_data)
        if n > 1:
            if not self.din_data[n-2] == self.din_data[n-1]:
                self.offset_count += 1
                self.offset_sum += self.joint_data[n-1]
                self.offsetLabel.setText("%5.2f" % (self.offset_sum/self.offset_count))

    
    def processMsg(self, msg):
        self.addData(msg.get('time'), msg.get('joint'), msg.get('din'))

        self.updatePlot()


    def closeEvent(self, event):		
        self.worker.stop()
        self.worker.wait()

        data_out = pd.DataFrame()
        data_out['time'] = np.array(self.time_data)
        data_out['joint'] = np.array(self.joint_data)
        data_out['din'] = np.array(self.din_data)

        out_filename = self.lineEdit.text()
        data_out.to_csv(out_filename)


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    w = MyWidget()
    w.show()
    app.exec_() 