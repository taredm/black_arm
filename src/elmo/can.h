
#ifndef KCAN_H
#define KCAN_H

#include <canlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string>
#include <cstring>

namespace Elmo
{
    /*****************************
    ** For CanMsg conversion
    ** by Dongmin
    ******************************/
    typedef union {
        uint8_t u8[8];
        int16_t i16[4];
        int32_t i32[2];
        int64_t i64[1];
        float f[2];
    } CanData;

    typedef struct {
        uint16_t id;
        uint8_t dlc;
        CanData data;
    } CanMsg;


    /*****************************
    ** CAN Interface for kvaser
    ** by Dongmin
    ******************************/
    class KCan
    {
    public:
        KCan(int channel=0);
        ~KCan();
        int startCanBus();
        int stopCanBus();
        int sendMsg(CanMsg& outMsg);
        int readMsg(CanMsg& inMsg);
        int readMsgWait(CanMsg &inMsg, unsigned long timeout);
        inline bool isOK(){ return (hnd<0)?(false):(true); };

    private:
        void printErrorState(const std::string &funcName, canStatus &stat);
        canHandle hnd;
        //canStatus stat;
        //int channelCount;
        int channel;
    };
}


#endif