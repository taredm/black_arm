#include "elmo.h"

using namespace Elmo;

void ElmoCanOpen::setVel(uint16_t id, float vel, CanMsg &msg)
{
    msg.id = id;
}

void ElmoCanOpen::setTq(uint16_t id, float vel, CanMsg &msg)
{
    msg.id = 0x0300 | id;
    msg.dlc = 8;
    msg.data.i64[0] = 0;
    msg.data.u8[0] = 'T';
    msg.data.u8[1] = 'C';

    msg.data.f[1] = vel;
    msg.data.u8[3] = 0x80;
}

void ElmoCanOpen::setPos(uint16_t id, float q, CanMsg &msg)
{
    msg.id = 0x0300 | id;
    msg.dlc = 8;
    msg.data.i64[0] = 0;
    msg.data.u8[0] = 'P';
    msg.data.u8[1] = 'A';

    int pos = q * 1137.778;
    msg.data.i32[1] = pos;
}

double ElmoCanOpen::getVel(const CanMsg &msg)
{
}

double ElmoCanOpen::getPos(const CanMsg &msg)
{
    return msg.data.i32[1]/1137.778;
}

void ElmoCanOpen::requestPos(uint16_t id, CanMsg &msg)
{
    msg.id = 0x0300 | id;
    msg.dlc = 4;
    msg.data.i64[0] = 0;
    msg.data.u8[0] = 'P';
    msg.data.u8[1] = 'X';
}

void ElmoCanOpen::initElmo1(uint16_t id, CanMsg &msg)
{
    msg.id = 0;
    msg.dlc = 8;
    msg.data.i64[0] = 0;
    msg.data.u8[0] = 0x82;
    msg.data.u8[1] = (uint8_t) id;

}

void ElmoCanOpen::initElmo2(uint16_t id, CanMsg &msg)
{
    msg.id = 0;
    msg.dlc = 8;
    msg.data.i64[0] = 0;
    msg.data.u8[0] = 0x01;
    msg.data.u8[1] = (uint8_t) id;
}

void ElmoCanOpen::setElmoStart(uint16_t id, CanMsg &msg)
{
    msg.id = 0x0300 | id;
    msg.dlc = 8;
    msg.data.i64[0] = 0;
    msg.data.u8[0] = 0x4d;
    msg.data.u8[1] = 0x4f;
    msg.data.u8[4] = 0x01;
}

void ElmoCanOpen::setElmoStop(uint16_t id, CanMsg &msg)
{
    msg.id = 0x0300 | id;
    msg.dlc = 8;
    msg.data.i64[0] = 0;
    msg.data.u8[0] = 0x4d;
    msg.data.u8[1] = 0x4f;
}

void ElmoCanOpen::setMode(uint16_t id, ElmoCanOpen::ELMO_CONTROL_MODE mode, CanMsg &msg)
{
    msg.id = 0x0300 | id;
    msg.dlc = 8;
    msg.data.i64[0] = 0;
    msg.data.u8[0] = 0x55;
    msg.data.u8[1] = 0x4d;
    if(mode == ELMO_POSITION)
        msg.data.u8[4] = 5;
    else if(mode == ELMO_CURRENT)
        msg.data.u8[4] = 1;
}

void ElmoCanOpen::begin(uint16_t id, CanMsg &msg)
{
    msg.id = 0x0300 | id;
    msg.dlc = 4;
    msg.data.i64[0] = 0;
    msg.data.u8[0] = 'B';
    msg.data.u8[1] = 'G';
}

void ElmoCanOpen::requestDigitalInput(uint16_t id, CanMsg &msg)
{
    msg.id = 0x0300 | id;
    msg.dlc = 4;
    msg.data.i64[0] = 0;
    msg.data.u8[0] = 'I';
    msg.data.u8[1] = 'P';
}

void ElmoCanOpen::setDIO(uint16_t id, CanMsg &msg)
{
    msg.id = 0x0300 | id;
    msg.dlc = 8;
    msg.data.i64[0] = 0;
    msg.data.u8[0] = 'I';
    msg.data.u8[1] = 'L';
    msg.data.u8[2] = 3;
    msg.data.i32[1] = 7;
}