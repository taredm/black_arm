#include <can.h>

using namespace Elmo;

KCan::KCan(int channel):
channel(channel)
{
    printf("Kvaser-Can Start...\n");
    canInitializeLibrary();

    hnd = canOpenChannel(channel, canOPEN_EXCLUSIVE | canOPEN_REQUIRE_EXTENDED | canOPEN_ACCEPT_VIRTUAL);
    if (hnd < 0) {
        printf("canOpenChannel %d", channel);
        printErrorState("", (canStatus &)hnd);
    }
}

KCan::~KCan()
{
    printf("Kvaser-Can End...\n");
    canStatus stat;

    if(!(hnd<0))
    {
        stat = canClose(hnd);
        printErrorState("canClose", stat);
    } 
    
    stat = canUnloadLibrary();
    printErrorState("canUnloadLibrary", stat);
    
}

int KCan::startCanBus()
{
    if(hnd<0)
    {
        printf("CAN channel is not opened...\n");
        return -1;
    }

    canStatus stat = canSetBusParams(hnd, canBITRATE_1M, 0, 0, 0, 0, 0);
    printErrorState("canSetBusParams", stat);
    if (stat != canOK) {
        return -1;
    }
    stat = canBusOn(hnd);
    printErrorState("canBusOn", stat);
    if (stat != canOK) {
        return -1;
    }

    return (int) stat;
}

int KCan::stopCanBus()
{
    if(hnd<0)
    {
        printf("CAN channel is not opened...\n");
        return -1;
    }

    canStatus stat = canBusOff(hnd);
    printErrorState("canBusOff", stat);

    return (int) stat;
}

int KCan::readMsg(CanMsg &inMsg)
{
    if(hnd<0)
    {
        printf("CAN channel is not opened...\n");
        return -1;
    }

    long id;
    unsigned char msg[8];
    unsigned int dlc;
    unsigned int flag;
    unsigned long time;

    // If the 'timeout' is -1, it will wait infinitely
    // timeout: wait ? milliseconds
    canStatus stat = canRead(hnd, &id, &msg, &dlc, &flag, &time);
    if(stat == canOK)
    {
        inMsg.id = id;
        inMsg.dlc = dlc;
        std::memcpy(&inMsg.data.u8, msg, sizeof(char)*8);
    }
   
    return (int) stat;
}

int KCan::readMsgWait(CanMsg &inMsg, unsigned long timeout=-1)
{
    if(hnd<0)
    {
        printf("CAN channel is not opened...\n");
        return -1;
    }

    long id;
    unsigned char msg[8];
    unsigned int dlc;
    unsigned int flag;
    unsigned long time;

    // If the 'timeout' is -1, it will wait infinitely
    // timeout: wait ? milliseconds
    canStatus stat = canReadWait(hnd, &id, &msg, &dlc, &flag, &time, timeout);
    if(stat == canOK)
    {
        inMsg.id = id;
        inMsg.dlc = dlc;
        std::memcpy(inMsg.data.u8, msg, sizeof(char)*8);
    }
    
    return (int) stat;
}

int KCan::sendMsg(CanMsg &outMsg)
{
    if(hnd<0)
    {
        printf("CAN channel is not opened...\n");
        return -1;
    }

    canStatus stat = canWrite(hnd, outMsg.id, outMsg.data.u8, outMsg.dlc, canMSG_STD);
    printErrorState("sendMsg", stat);

    return (int) stat;
}

void KCan::printErrorState(const std::string &funcName, canStatus &stat)
{
    if (stat != canOK) {
        char buf[50];
        buf[0] = '\0';
        canGetErrorText(stat, buf, sizeof(buf));
        printf("%s: failed, stat=%d (%s)\n", funcName.c_str(), (int)stat, buf);
    }
}