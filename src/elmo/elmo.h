#ifndef ELMO_H
#define ELMO_H

#include "can.h"

namespace Elmo
{
    class ElmoCanOpen{
    public:
        enum ELMO_CONTROL_MODE {ELMO_POSITION, ELMO_CURRENT};
        static void setVel(uint16_t id, float vel, CanMsg &msg);
        static void setTq(uint16_t id, float tq, CanMsg &msg);
        static void setPos(uint16_t id, float q, CanMsg &msg);
        static double getVel(const CanMsg &msg);
        static double getPos(const CanMsg &msg);
        static void requestPos(uint16_t id, CanMsg &msg);
        static void initElmo1(uint16_t id, CanMsg &msg);
        static void initElmo2(uint16_t id, CanMsg &msg);
        static void setElmoStart(uint16_t id, CanMsg &msg);
        static void setElmoStop(uint16_t id, CanMsg &msg);
        static void setMode(uint16_t id, ELMO_CONTROL_MODE mode, CanMsg &msg);
        static void begin(uint16_t id, CanMsg &msg);
        static void requestDigitalInput(uint16_t id, CanMsg &msg);
        static void setDIO(uint16_t id, CanMsg &msg);
    };
}

#endif