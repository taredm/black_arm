#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/mman.h>
#include <alchemy/task.h>
#include <alchemy/timer.h>
#include <alchemy/mutex.h>
#include <xenomai/init.h>
#include <math.h>

#include <can.h>
#include <elmo.h>


#define CLOCK_RES 1e-9 //Clock resolution is 1 ns by default
#define LOOP_PERIOD 1e8 //Expressed in ticks
#define ms(x)		(x*1000000) // milli seconds to nano seconds
#define MOTOR_ID	72

RT_TASK loop_task, recv_task;
bool EXIT_FLAG = false;
bool ELMO_READY = false;

void loop_task_proc(void *arg)
{
    //Print the message
    printf("Starting control loop....\n");

    //Make the task periodic with a specified loop period
    rt_task_set_periodic(NULL, TM_NOW, LOOP_PERIOD);

    // CAN message
    Elmo::CanMsg outMsg;

    // Initialize Elmo
    if( !((Elmo::KCan *)(arg))->isOK() )
    {
        printf("CAN Bus is not initialized!\n");
        EXIT_FLAG = true;
        return;
    }
    else
    {
        // init sequence.
        printf("ELMO initializing...\n");
        printf("\t-elmo_init1[%d]\n", MOTOR_ID);
        Elmo::ElmoCanOpen::initElmo1(MOTOR_ID, outMsg);
        int ret = ((Elmo::KCan *)(arg))->sendMsg(outMsg);
        if(ret!=canOK)
        {
            printf("send msg error\n");
            EXIT_FLAG = true;
            return;
        }
        rt_task_sleep(ms(50));

        printf("\t-elmo_init2[%d]\n", MOTOR_ID);
        Elmo::ElmoCanOpen::initElmo2(MOTOR_ID, outMsg);
        ret = ((Elmo::KCan *)(arg))->sendMsg(outMsg);
        if(ret!=canOK)
        {
            printf("send msg error\n");
            EXIT_FLAG = true;
            return;
        }
        rt_task_sleep(ms(50));

        printf("\t-elmo_stop[%d]\n", MOTOR_ID);
        Elmo::ElmoCanOpen::setElmoStop(MOTOR_ID, outMsg);
        ret = ((Elmo::KCan *)(arg))->sendMsg(outMsg);
        if(ret!=canOK)
        {
            printf("send msg error\n");
            EXIT_FLAG = true;
            return;
        }
        rt_task_sleep(ms(50));

        printf("\t-elmo_selectmode[%d]\n", MOTOR_ID);
        Elmo::ElmoCanOpen::setMode(MOTOR_ID, Elmo::ElmoCanOpen::ELMO_CONTROL_MODE::ELMO_CURRENT, outMsg);
        ret = ((Elmo::KCan *)(arg))->sendMsg(outMsg);
        if(ret!=canOK)
        {
            printf("send msg error\n");
            EXIT_FLAG = true;
            return;
        }
        rt_task_sleep(ms(50));

        printf("\t-elmo_start[%d]\n", MOTOR_ID);
        Elmo::ElmoCanOpen::setElmoStart(MOTOR_ID, outMsg);
        ret = ((Elmo::KCan *)(arg))->sendMsg(outMsg);
        if(ret!=canOK)
        {
            printf("send msg error\n");
            EXIT_FLAG = true;
            return;
        }
        rt_task_sleep(ms(50));

        printf("\t-elmo_tq[%d]\n", MOTOR_ID);
        Elmo::ElmoCanOpen::setTq(MOTOR_ID, 0.0, outMsg);
        ret = ((Elmo::KCan *)(arg))->sendMsg(outMsg);
        if(ret!=canOK)
        {
            printf("send msg error\n");
            EXIT_FLAG = true;
            return;
        }
        rt_task_sleep(ms(300));

        printf("ELMO initialization finished...\n");
        ELMO_READY = true;
    }


    //Start the task loop
    while(!EXIT_FLAG)
    {
        rt_task_wait_period(NULL);

        // request joint position
        Elmo::ElmoCanOpen::requestPos(MOTOR_ID, outMsg);
        int ret = ((Elmo::KCan *)(arg))->sendMsg(outMsg);
        if(ret!=canOK)
        {
            printf("send msg error\n");
            EXIT_FLAG = true;
            return;
        }
    }
}

void recv_task_proc(void *arg)
{
    printf("Starting recv loop....\n");
    Elmo::CanMsg inMsg;
    inMsg.data.i64[0] = 0;

    while(!EXIT_FLAG)
    {
        int ret = ((Elmo::KCan *)(arg))->readMsgWait(inMsg,-1);
        int elmoID = inMsg.id & 0x004f;
        if((ret == canOK) && (elmoID == MOTOR_ID) && ELMO_READY)
        {
            double q = Elmo::ElmoCanOpen::getPos(inMsg);
            printf("id: %5d, dlc: %5d, joint angle: %5.2lf \r", elmoID, inMsg.dlc, q);
        }
    }
}

void sig_handler(int sig)
{
    EXIT_FLAG = true;
}

int main(int argc, char *const *argv)
{
    // if you comment out a below line, it will cause a segmentation fault.
    xenomai_init(&argc, &argv);

    // set signal handler
    signal(SIGTERM, sig_handler);
    signal(SIGINT, sig_handler);

    //Lock the memory to avoid memory swapping for this program
    mlockall(MCL_CURRENT | MCL_FUTURE);

    printf("Test - Read Position from Elmo \n");

    // KCAN Interface
    Elmo::KCan *kcan = new Elmo::KCan();
    kcan->startCanBus();

    //Create the real time task
    rt_task_create(&loop_task, "control", 0, 50, 0);
    rt_task_create(&recv_task, "recv", 0, 70, 0);

    //Since task starts in suspended mode, start task
    rt_task_start(&recv_task, &recv_task_proc, (void *)kcan);
    rt_task_start(&loop_task, &loop_task_proc, (void *)kcan);

    //Wait for Ctrl-C
    pause();
    rt_task_join(&loop_task);
    rt_task_join(&recv_task);
        
    kcan->stopCanBus();
    delete kcan;

    printf("\nFinished...\n");

    return 0;
}