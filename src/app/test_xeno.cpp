#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/mman.h>
#include <alchemy/task.h>
#include <alchemy/timer.h>
#include <alchemy/mutex.h>
#include <xenomai/init.h>
#include <math.h>


#define CLOCK_RES 1e-9 //Clock resolution is 1 ns by default
#define LOOP_PERIOD 1e8 //Expressed in ticks
//RTIME period = 1000000000;
RT_TASK loop_task;

void loop_task_proc(void *arg)
{
    RT_TASK *curtask;
    RT_TASK_INFO curtaskinfo;
    int iret = 0;

    RTIME tstart, now;

    curtask = rt_task_self();
    rt_task_inquire(curtask, &curtaskinfo);
    int ctr = 0;

    //Print the info
    printf("Starting task %s with period of 10 ms ....\n", curtaskinfo.name);

    //Make the task periodic with a specified loop period
    rt_task_set_periodic(NULL, TM_NOW, LOOP_PERIOD);

    tstart = rt_timer_read();

    //Start the task loop
    while(1){
        now = rt_timer_read();
        printf("Loop count: %7d, Loop time: %10.5f ms\r", ctr, (now - tstart)/1000000.0);
        ctr++;
        rt_task_wait_period(NULL);

        tstart = now;
    }
}

int main(int argc, char *const *argv)
{
    // if you comment out a below line, it will cause a segmentation fault.
    xenomai_init(&argc, &argv);

    char str[20];

    //Lock the memory to avoid memory swapping for this program
    mlockall(MCL_CURRENT | MCL_FUTURE);

    printf("Starting cyclic task...\n");

    //Create the real time task
    sprintf(str, "cyclic_task");
    rt_task_create(&loop_task, "test", 0, 50, 0);

    //Since task starts in suspended mode, start task
    rt_task_start(&loop_task, &loop_task_proc, 0);

    //Wait for Ctrl-C
    pause();

    return 0;
}