#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <alchemy/task.h>
#include <alchemy/timer.h>
#include <alchemy/mutex.h>
#include <xenomai/init.h>

//#include <alchemy/pipe.h>
//If you use the mercury, you will see build errors.

#include <math.h>
#include <can.h>
#include <elmo.h>


#define CLOCK_RES 1e-9 //Clock resolution is 1 ns by default
#define LOOP_PERIOD 2e6 //Expressed in ticks
#define ms(x)		(x*1000000) // milli seconds to nano seconds
#define MOTOR_ID	74

RT_TASK loop_task, recv_task;
int pipe_fd;    // user-mode pipe
bool EXIT_FLAG = false;
bool ELMO_READY = false;

typedef struct {
    uint32_t type; // 0:joint, 1:din
    uint32_t din;
    double value;
    double timestamp;
} PipeMsg;

void loop_task_proc(void *arg)
{
    //Print the message
    printf("Starting control loop....\n");

    //Make the task periodic with a specified loop period
    rt_task_set_periodic(NULL, TM_NOW, LOOP_PERIOD);

    // CAN message
    Elmo::CanMsg outMsg;

    // Initialize Elmo
    if( !((Elmo::KCan *)(arg))->isOK() )
    {
        printf("CAN Bus is not initialized!\n");
        EXIT_FLAG = true;
        return;
    }
    else
    {
        // init sequence.
        printf("ELMO initializing...\n");
        printf("\t-elmo_init1[%d]\n", MOTOR_ID);
        Elmo::ElmoCanOpen::initElmo1(MOTOR_ID, outMsg);
        int ret = ((Elmo::KCan *)(arg))->sendMsg(outMsg);
        if(ret!=canOK)
        {
            printf("send msg error\n");
            EXIT_FLAG = true;
            return;
        }
        rt_task_sleep(ms(50));

        printf("\t-elmo_init2[%d]\n", MOTOR_ID);
        Elmo::ElmoCanOpen::initElmo2(MOTOR_ID, outMsg);
        ret = ((Elmo::KCan *)(arg))->sendMsg(outMsg);
        if(ret!=canOK)
        {
            printf("send msg error\n");
            EXIT_FLAG = true;
            return;
        }
        rt_task_sleep(ms(50));

        printf("\t-elmo_stop[%d]\n", MOTOR_ID);
        Elmo::ElmoCanOpen::setElmoStop(MOTOR_ID, outMsg);
        ret = ((Elmo::KCan *)(arg))->sendMsg(outMsg);
        if(ret!=canOK)
        {
            printf("send msg error\n");
            EXIT_FLAG = true;
            return;
        }
        rt_task_sleep(ms(50));

        printf("\t-elmo_selectmode[%d]\n", MOTOR_ID);
        Elmo::ElmoCanOpen::setMode(MOTOR_ID, Elmo::ElmoCanOpen::ELMO_CONTROL_MODE::ELMO_CURRENT, outMsg);
        ret = ((Elmo::KCan *)(arg))->sendMsg(outMsg);
        if(ret!=canOK)
        {
            printf("send msg error\n");
            EXIT_FLAG = true;
            return;
        }
        rt_task_sleep(ms(50));

        printf("\t-elmo_setDIO[%d]\n", MOTOR_ID);
        Elmo::ElmoCanOpen::setDIO(MOTOR_ID, outMsg);
        ret = ((Elmo::KCan *)(arg))->sendMsg(outMsg);
        if(ret!=canOK)
        {
            printf("send msg error\n");
            EXIT_FLAG = true;
            return;
        }
        rt_task_sleep(ms(50));

        printf("\t-elmo_start[%d]\n", MOTOR_ID);
        Elmo::ElmoCanOpen::setElmoStart(MOTOR_ID, outMsg);
        ret = ((Elmo::KCan *)(arg))->sendMsg(outMsg);
        if(ret!=canOK)
        {
            printf("send msg error\n");
            EXIT_FLAG = true;
            return;
        }
        rt_task_sleep(ms(50));

        printf("\t-elmo_tq[%d]\n", MOTOR_ID);
        Elmo::ElmoCanOpen::setTq(MOTOR_ID, 0.0, outMsg);
        ret = ((Elmo::KCan *)(arg))->sendMsg(outMsg);
        if(ret!=canOK)
        {
            printf("send msg error\n");
            EXIT_FLAG = true;
            return;
        }
        rt_task_sleep(ms(300));

        printf("ELMO initialization finished...\n");
        ELMO_READY = true;
    }


    //Start the task loop
    while(!EXIT_FLAG)
    {
        rt_task_wait_period(NULL);

        // request joint position
        Elmo::ElmoCanOpen::requestPos(MOTOR_ID, outMsg);
        int ret = ((Elmo::KCan *)(arg))->sendMsg(outMsg);
        if(ret!=canOK)
        {
            printf("send msg error\n");
            EXIT_FLAG = true;
            return;
        }

        // request  digital input
        Elmo::ElmoCanOpen::requestDigitalInput(MOTOR_ID, outMsg);
        ret = ((Elmo::KCan *)(arg))->sendMsg(outMsg);
        if(ret!=canOK)
        {
            printf("send msg error\n");
            EXIT_FLAG = true;
            return;
        }
    }
}

void recv_task_proc(void *arg)
{
    printf("Starting recv loop....\n");
    
    RTIME tstart;
    PipeMsg pMsg;
    Elmo::CanMsg inMsg;

    inMsg.data.i64[0] = 0;  
    tstart = rt_timer_read();

    while(!EXIT_FLAG)
    {
        int ret = ((Elmo::KCan *)(arg))->readMsgWait(inMsg,-1);
        int elmoID = inMsg.id & 0x004f;
        if((ret == canOK) && (elmoID == MOTOR_ID) && ELMO_READY)
        {
            if(inMsg.data.u8[0] == 'P' && inMsg.data.u8[1] == 'X')
            {
                double q = Elmo::ElmoCanOpen::getPos(inMsg);
                //printf("id: %5d, size: %5lu, joint angle: %5.2lf \r", elmoID, sizeof(PipeMsg), q);
                pMsg.type = 0;
                pMsg.value = q;
                pMsg.timestamp = (rt_timer_read() - tstart)/1000000000.0;
                write(pipe_fd, &pMsg, sizeof(PipeMsg));
            }
            else if(inMsg.data.u8[0] == 'I' && inMsg.data.u8[1] == 'P')
            {
                pMsg.type = 1;
                pMsg.din = (uint32_t) inMsg.data.i32[1];
                write(pipe_fd, &pMsg, sizeof(PipeMsg));
            }  
        }
    }
}

void sig_handler(int sig)
{
    EXIT_FLAG = true;
}

int main(int argc, char *const *argv)
{
    // if you comment out a below line, it will cause a segmentation fault.
    xenomai_init(&argc, &argv);

    // set signal handler
    signal(SIGTERM, sig_handler);
    signal(SIGINT, sig_handler);

    //Lock the memory to avoid memory swapping for this program
    mlockall(MCL_CURRENT | MCL_FUTURE);

    printf("Test - Read Position from Elmo \n");

    // Create Pipe to communicate with external apps.
    std::string myfifo = "/tmp/pipe_test";
    struct stat buf;
    if(stat(myfifo.c_str(), &buf) != 0)
    {
        if(mkfifo(myfifo.c_str(), 0666) == -1)
        {
            printf("ERROR: Fail to create fifo()\n");
            return -1;
        }
    }

    // open pipe
    pipe_fd = open(myfifo.c_str(), O_RDWR);
    if (pipe_fd < 0)
    {
        printf("ERROR: Fail to open the pipe.");
        return -1;
    }
    // KCAN Interface
    Elmo::KCan *kcan = new Elmo::KCan();
    kcan->startCanBus();

    //Create the real time task
    rt_task_create(&loop_task, "control", 0, 50, 0);
    rt_task_create(&recv_task, "recv", 0, 70, 0);

    //Since task starts in suspended mode, start task
    rt_task_start(&recv_task, &recv_task_proc, (void *)kcan);
    rt_task_start(&loop_task, &loop_task_proc, (void *)kcan);

    //Wait for Ctrl-C
    pause();
    rt_task_join(&loop_task);
    rt_task_join(&recv_task);
        
    kcan->stopCanBus();
    delete kcan;

    printf("\nFinished...\n");

    close(pipe_fd);
    return 0;
}