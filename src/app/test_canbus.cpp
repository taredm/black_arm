#include <stdio.h>
#include <can.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
    printf("test\n");
    Elmo::KCan kcan;
    
    kcan.startCanBus();
    usleep(500000);

    Elmo::CanMsg msg;
    kcan.sendMsg(msg);
    usleep(500000);
    
    kcan.stopCanBus();

    printf("exit\n");
    return 0;
}